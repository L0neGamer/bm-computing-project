# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* My computing project - timetable and miscellaneous spreadsheets for an exam officer
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Get visual studio
* Clone the repository
* Open the Solution file
* Make edits
* Commit, and sync

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* benjaminbwm@gmail.com
* Benjamin McRae
* Mr Crocker/West
* Computing department at JFS