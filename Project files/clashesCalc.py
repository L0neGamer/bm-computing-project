import readCSVtoList

def clashesReadForm(clashesFile = "Clashes"): # define the module with an input of the filename for clashes
    rawClashesFile = readCSVtoList.fileToList(clashesFile) # create a list of all the lines in the file
    clashesSectionBegin = [] # create two blank lists
    clashesSectionEnd = []
    for row in range(len([line for line in rawClashesFile])): # for each line in rawClashesFile, run this 
        for column in range(len([line for line in rawClashesFile][row])): # for each column in each row
            if rawClashesFile[row][column] == "Season: ": # if the item is "Season: "
                clashesSectionBegin.append((row,column)) # append the location of the row and column of this item to the clashesSectionBegin list
                break # break the column location for loop
            elif "Printed on :" in rawClashesFile[row][column]: # if the item is "Printed on :"
                clashesSectionEnd.append(row) # append the location of the row and column of this item to the clashesSectionEnd list
                break # break the column location for loop

    # pull the required items
    allClashes = {} # define an empty dictionary of all the clashes
    unorganisedClashes = [] # define an empty list for the unorganised clashes
    for sectionIndex in range(len(clashesSectionBegin)): # for each item in the clashesSectionBegin
        startRow = clashesSectionBegin[sectionIndex][0] # set the start row 
        endRow = clashesSectionEnd[sectionIndex] # set the end row
        startCol = clashesSectionBegin[sectionIndex][1] # set the start column
        neededCol = [1,4,6,8,5] # set which columns are wanted
        for row in rawClashesFile[startRow+6:endRow]: # for each row in the region
            unorganisedClashes += [[row[startCol+column] for column in neededCol]] # append all items in the row

    # organise the student's clashes into dictionaries with dates, times and studentIDs as the keys
    for rowIndex in range(len(unorganisedClashes)): # for each row index in the unorganised clashes
        if unorganisedClashes[rowIndex][0] != "": # if the first item is not empty
            date = tuple(unorganisedClashes[rowIndex][0].split("/")[::-1]) # set the date to be a tuple
            if unorganisedClashes[rowIndex][1].strip() == '08:30AM': # if the time is in the morning
                timeOfDay = "am" # set the timeOfDay to be in the morning
            else: # else
                timeOfDay = "pm" # set the timeOfDay to be in the afternoon
            studentID = (unorganisedClashes[rowIndex][2],unorganisedClashes[rowIndex][4]) # create a studentID by combining the student code and their name
            if date in allClashes.keys(): # if the date is in the keys for all the clashes
                if timeOfDay in allClashes[date].keys(): # if the current time is in the keys of the current date
                    pass # do nothing
                else: # else
                    allClashes[date][timeOfDay] = {} # create an empty dictionary for the current time and date
            else: # else
                allClashes[date] = {} # create an empty dictionary for current date
                allClashes[date][timeOfDay] = {} # create an empty dictionary for the current time and date
            allClashes[date][timeOfDay][studentID] = [unorganisedClashes[rowIndex][3]] # set the studentID's value in the dictionary to a list containing the exam code of an exam
        else: # else
            allClashes[date][timeOfDay][studentID].append(unorganisedClashes[rowIndex][3]) # append the value of the exam code of an exam to the list of the student's examclashes

    # find any items which are similar, and the student shouldn't have clashes in (for example, language listening, reading and writing exams)
    itemsToRemove = [] # create a list of the items that are due to be removed
    for date in sorted(allClashes.keys()): # for each date in the keys of allClashes
        for timeOfDay in sorted(allClashes[date].keys()): # for each timeOfDay on this particular day
            for studentID in sorted(allClashes[date][timeOfDay].keys()): # for each student which has a clash at this time on this day
                for examCodeIndex in range(len(allClashes[date][timeOfDay][studentID])): # for each examCodeIndex which this student has a clash in at this time on this day
                    for examCode in (item for index,item in enumerate(allClashes[date][timeOfDay][studentID]) if index!=examCodeIndex): # for each exam code out of all the clashes except the exam of examCodeIndex
                        if allClashes[date][timeOfDay][studentID][examCodeIndex][:4] == examCode[:4]: # if the exam at the particular index shares the first four characters of the second iteration of the examCode
                            itemsToRemove.append((date, timeOfDay, studentID, examCodeIndex)) # append the location of the examCodeIndex to itemsToRemove

    #with help from https://www.peterbe.com/plog/uniqifiers-benchmark
    # removing repeats in itemsToRemove
    seen={}
    result=[]
    for item in itemsToRemove:
        if item in seen: continue
        seen[item] = 1
        result.append(item)
    itemsToRemoveTidy = result
    
    for excessClash in itemsToRemoveTidy[::-1]: # for each excess clash
        allClashes[excessClash[0]][excessClash[1]][excessClash[2]].pop(excessClash[3]) # remove the particular index it is at
        if allClashes[excessClash[0]][excessClash[1]][excessClash[2]] == []: # if the student's list of clashes is empty
            allClashes[excessClash[0]][excessClash[1]].pop(excessClash[2]) # remove the student who has these clashes
            if allClashes[excessClash[0]][excessClash[1]] == {}: # if the timeOfDay is empty
                allClashes[excessClash[0]].pop(excessClash[1]) # remove the timeOfDay
                if allClashes[excessClash[0]] == {}: # if the date is empty
                    allClashes.pop(excessClash[0]) # remove the  date

    # rearranging how the clashes are configured
    configuredClashes = {} # create an empty list
    for date in sorted(allClashes.keys()): # for each date
        for timeOfDay in sorted(allClashes[date].keys()): # for each time
            for studentID in sorted(allClashes[date][timeOfDay].keys()): # for each student
                examCodes = tuple(allClashes[date][timeOfDay][studentID]) # create a tuple of the exams they have clashes with
                if date in configuredClashes.keys(): # if the date is already in the keys of configuredClashes
                    if timeOfDay in configuredClashes[date].keys(): # if the timeOfDay is already in the keys of configuredClashes[date]
                        if not examCodes in configuredClashes[date][timeOfDay].keys(): # if the examCodes tuple is not already in the keys of configuredClashes[date][timeOfDay]
                            configuredClashes[date][timeOfDay][examCodes] = [] # set the list of students which have this clash to empty
                    else: # else
                        configuredClashes[date][timeOfDay] = {examCodes:[]} # create a dictionary which has the examCodes as a key and an empty list as a value
                else: # else
                    configuredClashes[date] = {timeOfDay:{examCodes:[]}} # create a dictionary which has the timeOfDay as a key and a dicitonary which has examCodes as a key and an empty list as a value, as a value
                configuredClashes[date][timeOfDay][tuple(allClashes[date][timeOfDay][studentID])].append(studentID) # append the student's studentID to this list

    # {date: {timeOfDay: {(examcodes): [student ids]}}}
    return(configuredClashes) # return configuredClashes
