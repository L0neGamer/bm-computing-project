import readCSVtoList, collateLists, xlsxwriter, clashesCalc

def widthSorting(columnWidths, column, width, sheet):
    if sheet in columnWidths:
        pass
    else:
        columnWidths[sheet] = {}
    if column in columnWidths[sheet].keys():
        if width > columnWidths[sheet][column]:
            columnWidths[sheet][column] = width
    else:
        columnWidths[sheet][column] = width
    return(columnWidths)


def outputFile(examsFile = 'full data', accArraFile = 'access arrangements',examDatesFile = "exam times and dates", outputFileName = "output", clashesFile = "Clashes"):
    examDetails, examData = collateLists.createInfoSheet(examsFile, accArraFile, examDatesFile)
    configuredClashes = clashesCalc.clashesReadForm(clashesFile)
    dates = sorted(examDetails.keys())[1:]
    workbook = xlsxwriter.Workbook(outputFileName+'.xlsx',{'constant_memory': True})
    dateFormat = workbook.add_format({'bold': True, 'font_size': 15})
    centeredFormat = workbook.add_format({'align':"center","font_name":"Consolas","border":1})
    borderedFormat = workbook.add_format({"border":1,"font_name":"Consolas"})
    noFormat = workbook.add_format({"font_name":"Consolas"})
    examCodesFrmData = sorted([exam[0] for exam in examData.keys()])
    #ET	RDR	RB	SF	SR	WP	Other

    needsWP = workbook.add_format({'font_color': '#7CD5F2',"border":1,"font_name":"Consolas"})
    needsSR = workbook.add_format({'font_color': '#E344E0',"border":1,"font_name":"Consolas"})
    needsRB = workbook.add_format({'font_color': '#A0522D',"border":1,"font_name":"Consolas"})
    needsNone = workbook.add_format({'font_color': '#000000',"border":1,"font_name":"Consolas"})
    needsSF = workbook.add_format({'font_color': '#1C9111',"border":1,"font_name":"Consolas"})
    accArraFormats = [needsRB,
                      needsSF,
                      needsSR,
                      needsWP]
    columnWidths = {}
    sheets = {}
    examKeys = {}
    currentRow = 0
    
    # exam codes and examkeys
    for examKey in examData.keys():
        examKeys[examKey[0]]=examKey

    for date in sorted(dateItem[::-1] for dateItem in dates):
        date = date[::-1]
        # infosheet generation
        stringDate = str(date[2])+"/"+str(date[1])+"/"+str(date[0])
        infoSheetName = "Infosheet "+str(date[2])+"|"+str(date[1])+"|"+str(date[0])
        infoSheet = workbook.add_worksheet(infoSheetName)
        sheets[infoSheetName] = infoSheet
        infoSheet.write('A1',stringDate,dateFormat)

        # column name creation
        columnNamesInfo = ["Start","Component Title","Comp Code","Length","Cands"] + [item.split(" ")[0] for item in examDetails[('0000', '00', '00')][0]['000000']]
        lengthOfTotals = len([item.split(" ")[0] for item in examDetails[('0000', '00', '00')][0]['000000']])+1
        infoSheetColWidths = [15,30,10.57,14.14,6,2.57,3.71] + [2.57 for i in range(len(columnNamesInfo)-8)] + [6] # widths of columns in infosheets

        for columnNo in range(len(columnNamesInfo)):
            column = chr(columnNo+65)
            columnWidths = widthSorting(columnWidths, column+":"+column, infoSheetColWidths[columnNo], infoSheetName)
            infoSheet.write(1,columnNo,columnNamesInfo[columnNo],borderedFormat)

        currentRow = 1
        # infosheet generation
        for timeOfDay in ["am","pm"]:
            unstrippedCodes = sorted((item for item in examCodesFrmData))
            strippedCodes = [item[:4].strip(" /") for item in unstrippedCodes]
            if timeOfDay == "pm":currentRow+=1
            totalNums = [0 for number in range(lengthOfTotals)]
            examCodes = sorted(list(examDetails[date][0].keys()))
            for examCodeIndex in range(len(examCodes)):
                if not(examCodes[examCodeIndex][:4].strip(" /") in [exam[0][:4].strip(" /") for exam in examData.keys()]):
                    examDetails[date][0].pop(examCodes[examCodeIndex])
                       
            for examCode in examCodes:
                if examCode[:4].strip(" /") in (exam[:4].strip(" /") for exam in examCodesFrmData):
                    generateInfo = False
                    for examTime in examDetails[date][1][timeOfDay]:
                        if examCode in examTime:
                            generateInfo = True
                    if generateInfo:

                        if timeOfDay == "am":startTime="8:30"
                        elif timeOfDay == "pm":startTime="13:30"

                        compTitle = "unknown"
                        indexOfExamCode = [exam[0] for exam in examDetails[date][1][timeOfDay]].index(examCode)
                        compTitle = examDetails[date][1][timeOfDay][indexOfExamCode][2]
                        examCandidates =examDetails[date][1][timeOfDay][indexOfExamCode][3]
                        examLength = examDetails[date][1][timeOfDay][indexOfExamCode][1]

                        rowData = [startTime, compTitle, examCode, examLength, examCandidates] + examDetails[date][0][examCode]
                        toAddToTotal = [int(examCandidates)] + examDetails[date][0][examCode]
                        # ['Start', 'Component Title', 'Comp Code', 'Length', 'Cands', ET	RDR	RB	SF	SR	WP	Other]

                        currentRow += 1 
                        for columnNo in range(len(rowData)):
                            column = chr(columnNo+65)
                            if columnNo>=4:
                                chosenFormat=centeredFormat
                            else:
                                chosenFormat=borderedFormat

                            infoSheet.write(currentRow,columnNo,rowData[columnNo],chosenFormat)
                        for column in range(len(totalNums)):
                            totalNums[column] += toAddToTotal[column]
                        strippedCodes.pop(indexOfExamCode)
                        unstrippedCodes.pop(indexOfExamCode)

            currentRow +=1
            totalNums = ["Total for " + timeOfDay] + totalNums
            for columnNo in range(len(totalNums)):
                column = chr(columnNo+65+3)
                infoSheet.write(currentRow,columnNo+3,totalNums[columnNo],centeredFormat)

        lastCol = columnNo+3 # define the last column
        # end of main infosheet output

        currentRow += 5
        infoSheet.write(currentRow,0,"Clashes",noFormat)
        clashesDate = date

        if clashesDate in configuredClashes.keys():
            for timeOfDay in sorted(configuredClashes[clashesDate].keys()):
                for examClash in sorted(configuredClashes[clashesDate][timeOfDay].keys()):
                    currentRow +=1
                    infoSheet.write(currentRow,0,timeOfDay.upper(),noFormat)
                    infoSheet.write(currentRow,1,', '.join(examClash),noFormat)
                    currentRow+=1
                    infoSheet.write(currentRow,0,"Preferred solution:",noFormat)
                    currentRow+=1
                    for student in configuredClashes[clashesDate][timeOfDay][examClash]:
                        infoSheet.write(currentRow,0,student[0],noFormat)
                        infoSheet.write(currentRow,1,student[1],noFormat)
                        currentRow += 1

        lastRow = currentRow # define the last row
        infoSheet.print_area(0, 0, lastRow, lastCol) # set the print area to this area
        infoSheet.fit_to_pages(1,0) # set the fit to be one page wide by however many pages tall 
        infoSheet.set_paper(9) # set paper size to A4

        # register generation
        unstrippedCodes = [item for item in examCodesFrmData]
        strippedCodes = [item[:4].strip(" /") for item in unstrippedCodes]
        stringDate = str(date[2])+"/"+str(date[1])+"/"+str(date[0])

        for examCode in sorted(examDetails[date][0].keys()):
            completeListOfExamDetails = [exam for exam in examDetails[date][1]["am"]] + [exam for exam in examDetails[date][1]["pm"]]
            completeListOfExamCodes = [exam[0] for exam in completeListOfExamDetails]
            indexOfExamCode = completeListOfExamCodes.index(examCode)
            compTitle = completeListOfExamDetails[indexOfExamCode][2]
            dataToInput = []
            indexOfExamCodeData = strippedCodes.index(examCode[:4].strip(" /"))
            accessArraLocation = 0
            
            for student in examData[examKeys[unstrippedCodes[indexOfExamCodeData]]]:
                if all((i == "" for i in student[6:])):
                    dataToInput.append([stringDate[:-5],student[4],student[0],student[1],student[3],examCode,compTitle])
                else:
                    dataToInput.insert(accessArraLocation,[stringDate[:-5],student[4],student[0],student[1],student[3],examCode,compTitle]+student[6:])
                    accessArraLocation +=1 

            currentRegisterName = examCode + " - " + stringDate.replace("/","|")
            currentRegister = workbook.add_worksheet(currentRegisterName.replace("/"," "))
            currentRow = 0
            columnNamesReg = ["Date","Cand","Surname","Forename","TG","Code","Exam Name"] + [item.split(" ")[0] for item in examDetails[('0000', '00', '00')][0]['000000']]
            for columnNo in range(len(columnNamesReg)):
                column = chr(columnNo+65)
                columnWidths = widthSorting(columnWidths, column+":"+column, len(columnNamesReg[columnNo])*1.14+0.29, currentRegisterName)
                currentRegister.write(0,columnNo,columnNamesReg[columnNo],borderedFormat)

            #"Date","Candidate","Surname","Forename","TG","Code","Exam Name" ET	RDR	RB	SF	SR	WP	Other

            sheets[currentRegisterName] = currentRegister

            for student in dataToInput:
                student = list(student)
                currentRow += 1
                studAccArras = student[7:13]
                lineFormat = borderedFormat

                if studAccArras != []:
                    studAccArras.pop(1)
                    studAccArras.pop(0)
                    for accArraIndex in range(len(studAccArras)):
                        if studAccArras[accArraIndex] != "":
                            accArraFormatsFormatted = accArraFormats
                            lineFormat = accArraFormatsFormatted[accArraIndex]

                columnToWrite=0
                for columnNo in range(len(columnNamesReg)):
                    if not(columnNo in (99,100)):
                        column = chr(columnNo+65)
                        try:
                            columnWidths = widthSorting(columnWidths, column+":"+column, len(str(student[columnNo]))*1.14+0.29,currentRegisterName)
                            currentRegister.write(currentRow,columnNo,student[columnNo],lineFormat)
                        except IndexError:
                            currentRegister.write(currentRow,columnNo,"",lineFormat)
                        columnToWrite+=1

            lastRowRegister = currentRow
            lastColRegister = columnNo
            currentRegister.repeat_rows(0) # repeat the first row
            currentRegister.print_area(0, 0, lastRowRegister, lastColRegister) # set the print area
            currentRegister.fit_to_pages(1,0) # set the fit to be one page wide by however many pages tall 
            currentRegister.set_paper(9) # set paper size to A4

            strippedCodes.pop(indexOfExamCodeData)
            unstrippedCodes.pop(indexOfExamCodeData)

    # end of file output

    for sheetName in columnWidths.keys():
        for column in columnWidths[sheetName].keys():
            sheets[sheetName].set_column(column,columnWidths[sheetName][column])

    workbook.close()
