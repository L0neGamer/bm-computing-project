# Module list to ordered collation
import readCSVtoList
def collateArrangements(examsFile = 'full data', accArraFile = 'access arrangements'): # put the student's exam data and access arrangements together
    examsDict = readCSVtoList.formatExamsList(examsFile) # bring in the formatted exams list
    accArraList, colHeads = readCSVtoList.formatAccessArraList(accArraFile) # bring in the access arrangement list

    #find a default for some values
    key = [key for key in examsDict.keys()][0] 
    student = examsDict[key][0]
    defaultStudentLen = len(student)
    accArraLen = len(accArraList[0][4:])
    colHeads = colHeads[4:]

    for student in enumerate(accArraList): # for each student with access arrangments
        for key in examsDict.keys(): # for each key
            examEntryCodes = [examsDict[key][index][4] for index in range(len(examsDict[key]))]
            if student[1][0] in examEntryCodes: # if the student is in the current exam
                examsDict[key][examEntryCodes.index(student[1][0])] += accArraList[student[0]][4:] # append their access arrangements to their student data in their exam

    for key in sorted(examsDict.keys()): # for each key in the examsdict
        for student in enumerate(examsDict[key]): # for each student in that exam
            if len(student[1]) == defaultStudentLen: # if the length of the student's data is the same as the default's
                for iterate in range(accArraLen): # for each item in the length of the access arrangments
                    examsDict[key][student[0]] += [""] # append empty spaces to the student
            elif len(student[1]) == (defaultStudentLen+accArraLen): # if the student has the same length as the defualt length+the number of access arrangements
                pass # do nothing
            else: # else
                print("That isn't supposed to happen") # be confused
    #ET (extra time 25%)	WP (word processor)	RB (rest breaks)	SR (separate room)	RDR (reader/prompter)	SF (sit at front)	Other (food, leave immediately, etc)
    return(examsDict,accArraLen,colHeads) # return the examsdict, number of access arrangements, and the columns headers

def collateExamTimes(examDatesFile = "exam times and dates", examFiles = "full data"): # put the exam times and dates together with the exam data
    examTimes = readCSVtoList.formatExamTimes(examDatesFile, examFiles) # pull the examtimes
    sortedDates = sorted([(examDate[1][1],examDate[0]) for examDate in enumerate(examTimes)]) # sort the dates
    examDate = {} # create an empty dictionary
    for date in sortedDates: # for each date
        toAppend = (examTimes[date[1]][0],examTimes[date[1]][2], examTimes[date[1]][3], examTimes[date[1]][4], examTimes[date[1]][5]) # create the item to append
        if not(date[0] in examDate.keys()): # if the current date isn't in the dictionary
            examDate[date[0]] = [toAppend] # create it and place the item in a list in it
        else: # else
            examDate[date[0]].append(toAppend) # append the item to the list
    return(examDate) # return the dictionary

def createInfoSheet(examsFile = 'full data', accArraFile = 'access arrangements',examDatesFile = "exam times and dates"): # tie together the two other modules in this module
    examData, accArraLen, colHeads = collateArrangements(examsFile, accArraFile) # grab these data
    examDate = collateExamTimes(examDatesFile, examsFile) # grab examDate
    examDetails ={("0000","00","00"): ({'000000': colHeads},{'pm': [], 'am': ['000000']})} # create a default entry into the examDetails dictionary
    examCodesFrmData = [exam[0] for exam in examData.keys()] # grab the exam codes from the examData
    examDataKeys = sorted((key for key in examData.keys())) # sort the keys for each exam
    for date in sorted(examDate.keys()): # for each date
        examTimes = {"am":[], "pm":[]} # create a default dictionary
        examAccArra = {} # create an empty dictionary
        for exam in sorted(examDate[date]): # for each exam on the current date
            examTimes[exam[1]].append((exam[0],exam[2],exam[3], exam[4])) # append to examtimes at a particular time particular data
            examAccArra[exam[0]] = [] # create an empty in the examAccArra dict
            toPop = [] # create an empty list
            accessArraNums = [0 for i in range(accArraLen)] # create a list of zeroes the length of the number of access arrangements
            keyToUse = None # set the key to use as none
            for keyIndex in range(len(examDataKeys)): # for each key in examData
                if exam[0][:4].strip(" /") == examDataKeys[keyIndex][0][:4].strip(" /"): # if the current exam code equals the examData exam code
                    keyToUse = examDataKeys[keyIndex] # set the key to use as the current one
                    toPop.append(keyIndex) # add the current key to the "topop" list
                    break # break the for loop
            if keyToUse != None: # if a key was set
                for student in examData[keyToUse]: # for each student in the current exam
                    for index in range(accArraLen): # for each index of accArraLen
                        if student[6:][index] != "": # if the item is not blank
                            accessArraNums[index] += 1 # iterate that number index by one in accessArraNums
            examAccArra[exam[0]] = accessArraNums # set the entry to equal accessArraNums
        examDetails[date] = (examAccArra,examTimes) # set the examdetails of the current date to equal the access arrangement numbers for that exam, and the current date's examTimes
    return(examDetails,examData) # return the examdetails and the examdata