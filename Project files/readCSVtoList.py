import time
import datetime

def fileToList(csvFileName): # create the function with a value to be passed to it
    from csv import reader as csv_reader # import the relevant module
    with open(csvFileName + ".csv","r") as csvFile: # open the file for reading
        readFile = [item for item in csv_reader(csvFile)] # make the readFile equal a list of each item in the csv generator
    return(readFile) # return readFile

def formatExamsList(examsFile = "full data"): # create the function, with a default value of a file of name "full data"
    formattedFile = fileToList(examsFile)
    examSectionsBegin = [] # create an empty list to store the locations of the exam sections, for use in the next for loop
    examSectionsEnd = []
    
    for itemIndex in range(len(formattedFile)): # for each line in formattedFile, run this 
        if formattedFile[itemIndex][0] == "Season :": # if the first item in the list is "Element Entry Listing"
            examSectionsBegin.append(itemIndex-1) # append the current index to the examSectionsBegin list
        elif formattedFile[itemIndex][0] == "Printed On" and formattedFile[itemIndex-3][3]!="": # if the first item is printed on:
            examSectionsEnd.append(itemIndex) # append current index to examsectionend list
            
    examEntries = {} # create empty dictionary
    for entryIndex in range(len(examSectionsBegin)): # for all "Element Entry Listing"
        entry = examSectionsBegin[entryIndex] # find the location that the current table is
        identification = formattedFile[entry+3][2],formattedFile[entry+1][2],formattedFile[entry+3][7],formattedFile[entry+1][7] # create the identification out of the exams code, season, exam name and qualification level

        studentsToAdd = formattedFile[entry+7:examSectionsEnd[entryIndex]] # create a list of all the students entries
        toAddToStudentsToAdd = [] # empty list to append items to
        for line in studentsToAdd: # for each entry in studentstoadd
            if not all(line[index] == "" for index in range(len(line)-5)): # if some of the items are not empty strings
                studentCode = line[8] # set the studentcode to the eighth item in the line
                if studentCode == "": # if this item is blank
                    studentCode = line[9] # set it to the ninth
                toAddToStudentsToAdd.append([line[0], line[3], line[5], line[6], studentCode, line[9]]) # append the required fields to this list
        studentsToAdd = toAddToStudentsToAdd # set studentstoadd to this list
        
        #studentsToAdd = [[line[0], line[3], line[5], line[6], line[8], line[9]] for line in studentsToAdd if not all(line[index] == "" for index in range(len(line)-5))] # edit studentsToAdd so that empty or "useless" columns aren't included, and empty rows. 
        # surname, forename, year group, tutor base, student number, "UCI"
        
        for line in enumerate(studentsToAdd): # gives item (line's index,line's contents) for each line
            for field in enumerate(line[1]): # gives item (field's index,field's contents) for each field
                if type(field[1]) == str: # if the item is a string
                    studentsToAdd[line[0]][field[0]] = field[1].strip(" ()") # strip leading and trailing spaces and brackets

        studentsToAdd = [line for line in studentsToAdd if line!=[] and line[0]!='Count of Entries :'] # removes empty lines and lines that start with 'Count of Entries :'
        
        if identification in examEntries: # if the key already is in the dict
            examEntries[identification] += studentsToAdd # append studentsToAdd to the item in the dict
        else: # otherwise
            examEntries[identification] = studentsToAdd # add an entry that is "studentsToAdd" with the key of identification

    return(examEntries) # return the examEntries dictionary

def formatAccessArraList(accArrFile = "access arrangements"): # create the function, with a default value of a file of name "access arrangements"
    formattedFile = fileToList(accArrFile) # retrieve the file of access arrangements
    #note: possible arrangements can include 0.25 Extra Time (ET), rest breaks(RB), separate room (SR), word processor (WP), reader/prompter (RDR),sit at front (SF), Other (food,"leave hall immediately")
    #this order: ET	RDR	RB	SF	SR	WP	Other
    studentsAcessArrange = [] # create an empty list for the next for loop 
    for record in formattedFile[1:]: # for each record in the file, do
        studentsAcessArrange.append(record) # append to the empty list each line
    return(studentsAcessArrange,formattedFile[0]) # return the list of access arrangements

def formatExamTimes(examDatesFile = "exam times and dates", examsFile = "full data"):# create the function, with a default value of a file of name "exam times and dates"
    formattedFile = fileToList(examDatesFile)[6:] # retrive the file of exam times and dates, and miss the first 6 characters
    examsRegistered = [exam for exam in formatExamsList(examsFile).keys()] # find the exam details of every exam that students have been registered for
    examCodeFile = [examRecord[4] for examRecord in formattedFile] # make a list of only the exam codes of the exam times file

    for record in enumerate(formattedFile): # for each entry
        wordedDate = record[1][0] # set the worded date to the first item in the line
        if wordedDate == "TBA": # if it's "tba"
            continue # skip this for loop iteration
        elif wordedDate == "": # elif it's empty
            break # end this for loop
        wordedDate += " " + str(datetime.datetime.now().year) # append the current year 
        dateToCompare = time.strftime("%Y/%m/%d",time.strptime(wordedDate,"%a %d %b %Y")).split("/") # split up the worded date into a tuple to compare
        currentDate = time.strftime("%Y/%m/%d").split("/") # create the current date into a tuple
        if dateToCompare > currentDate: # if the exam's date is in the future
            formattedFile[record[0]].append(True) # append true to it's entry
        elif dateToCompare == currentDate: # if the exam's date is today
            conditionalOutput("Warning! Exam", formattedFile[record[0]][0],"is today, according to the date/time file! Caution is advised") # output a message
            formattedFile[record[0]].append(True) # append true to it's entry
        elif dateToCompare < currentDate: # if the exam is before today
            conditionalOutput("Exam",formattedFile[record[0]][0],"has a date prior to today, so will not be included in the final spreadsheets") # output a message
            formattedFile[record[0]].append(False) # append false to it's entry

    #conditionalOutput(examCodeFile)
    #conditionalOutput([i for i in enumerate(exam[0] for exam in examsRegistered)])
    for examCode in enumerate(exam[0] for exam in examsRegistered): # for each (index, value) of the exam codes of registered exams, do
        timeOfDay = False # set the time of day to false, so that if not set an if statement is not run
        continueExecution = True # by default, continue excecution

        if not(examCode[1][:4].strip(" /") in (item[:4].strip(" /") for item in examCodeFile)): # if a registered exam code is not in the times file
            conditionalOutput("Exam with details '"+str(examsRegistered[examCode[0]])+"' does not have a time or date associated with it.\nPlease rectify this and restart the program") # tell the user the details of the exam without date and time information
            continueExecution = False # do not continue excecution
        
        toRemove = [] # create an empty list of items to remove
        for examCodeFromTimesIndex in range(len(examCodeFile)): # for each index of examcodes
            if not(examCodeFile[examCodeFromTimesIndex][:4].strip(" /") in [exam[0][:4].strip(" /") for exam in examsRegistered]): # if the currently being observed exam code isn't in the exams registered
                conditionalOutput("Exam with code '"+examCodeFile[examCodeFromTimesIndex]+"' does not have data associated with it.\nPlease rectify this and restart the program") # output a message
                toRemove.append(examCodeFromTimesIndex) # append the current examcodeindex to the toremove list
                continueExecution = False # do not continue with excecution
        for index in toRemove[::-1]: # for each item in toRemove (backwards)
            examCodeFile.pop(index) # remove that item from the examcodefile

        #conditionalOutput(continueExecution, examCode[1])
        if continueExecution: # else, do this
            examCodeIndex = [item[:4].strip(" /") for item in examCodeFile].index(examCode[1][:4].strip(" /")) # find the date related to the current exam code. note: may not work correctly if the exam code is entered more than once. will have to make sure the user doesn't do that
            if formattedFile[examCodeIndex][-1]:
                if formattedFile[examCodeIndex][1] == "8:30": # if the exam is am
                    timeOfDay = "morning" # set the time of day for that exam to be "morning"

                elif formattedFile[examCodeIndex][1] == "13:30": # else, if the exam is pm
                    timeOfDay = "afternoon" # set the time of day for that exam to be "afternoon"

                else: # if neither of these are true, do
                    conditionalOutput("There is no time, or an incorrect time, associated with "+ formattedFile[examCodeIndex][4] +". Please enter something in this slot") # give the user a message telling them that despite there being an exam entry for the exam, it has no time set

                if timeOfDay != False: # if the time of day has been set
                    conditionalOutput("Exam '"+examCode[1]+"' has a date of '" + formattedFile[examCodeIndex][0] + "' and should begin in the " + timeOfDay + ".") # print exam info. this will be removed in short time, as the print information is not required for file output

    return([(exam[4],
             tuple(time.strftime("%Y/%m/%d",time.strptime(exam[0]+" 17","%a %d %b %y")).split("/")),
             "am" if exam[1] == "08:30" else "pm",
             exam[2],
             exam[5],
             exam[7]) for exam in formattedFile if exam[-1] and not(exam[0] in ("TBA","","Where a component is linked to more than one element the level displayed will be the level of the first element that the system detects.","The number of candidates shown is for the component as whole."))]) # return a list of items, as long as certain conditions are met

def conditionalOutput(*args, output=False):
    if output:
        print(*args)
