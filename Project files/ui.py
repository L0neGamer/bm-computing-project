import tkinter as tk
from tkinter import messagebox
import threading, time, queue
from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
import outputsFiles
from os import system as command

class Loader(tk.Label):
    """A child class of a tkinter Label that shows a spinner when the start method is called
Use it like a tkinter widget"""
    # uses http://stackoverflow.com/questions/42168277/spinning-wheel-function-does-not-always-exit/42589973#42589973
    positions_text = '/-\\' # all the different characters that the spinner will display
    def __init__(self, parent, *args, **kwargs):
        kwargs['text']=''
        tk.Label.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.runnng = False # keeps track of whether it is running
        self.pos = 0 # the index in positions_text of the character being displayed
    def start(self):
        "Start the spinner spinning"
        self.running = True
        self.parent.after(0,self.update)

    def update(self):
        "A function for internal use that loops to update the spinner with the new text ever 100ms"
        self.config(text = self.positions_text[self.pos]) # change text to new character
        self.pos = (self.pos+1)%len(self.positions_text)
        if self.running:
            self.parent.after(100,self.update) # class this function again after 100ms
        else:
            self.config(text = '✓') # when finished, change the text to a tick
    def stop(self):
        "Stop the spinner spinning"
        self.running = False

def helpBox():
    messagebox.showinfo("Help", """Exams and Students file - From SIMS, has students and assigned qualifications
Access arrangements file - Manually created, MUST follow a specific format
Ouptut file name - Must be a Windows compatible file name
Clashes file - From SIMS, contains clashes information ordered by date then by student

For more information, contact Mrs Furman
For technical help, contact benjaminbwm@gmail.com""")
        
def submit(*textBoxes):
    "The function that is called when the submit button is clicked"
    global values
    # get the values from the textboxes
    values = []
    for textBox in textBoxes:
        values.append(textBox.get())

    if values[0] == "":
        values[0]  = 'full data'
    if values[1] == "":
        values[1] = 'access arrangements'
    if values[2] == "":
        values[2] = "exam times and dates"
    if values[3] == "":
        values[3] = "output"
    if values[4] == "":
        values[4] = "Clashes"
    # clear the screen
    for w in root.winfo_children():
            w.destroy()
    # create new ui
    tk.Label(root,text="Working",font=("Arial",50)).grid(row=0,column=0,columnspan=2,pady=20) # title
    loader = Loader(root) # spinner (as defined above)
    loader.grid(row=1,column=1)
    loader.start() # start the spinner spinning
    
    # start the function threadWorker in a separate thread, with arguments loader, val1,val2,val3,val4 and val5
    # NB: the loader argument is needed in order to stop the spinner when the task is finished
    threading.Thread(target=threadWorker,args=(loader,values)).start()

def threadWorker(loader,values):
    """The function that will be running in a separate thead
The outputFile function is called here because it takes a long time and if it were called
in the main thread like an ordinary function then it would freeze all the UI
tkinter is not thread-safe so no tkinter widgets or messageboxes can be created, destroyed, or accessed in any way
All communication with the rest of the program must be done via putting items in queue1"""
    try:
        outputsFiles.outputFile(*values)
        print("\a")
        loader.stop() # stop the spinner
        time.sleep(3)
        queue1.put("finished") # put the string "finished" in the queue so the UI can be changed accordingly
    except Exception as e:
        queue1.put(e) # if there is an error, put the error object in the queue
        loader.stop() # stop the spinner

def browseFiles(entry, save=False):
    "Set the value of a given textbox to the filepath of the chosen file"
    if save:
        # get the filepath returned by the filedialogue
        fname = asksaveasfilename(defaultextension="xlsx",filetypes=(("Excel files", "*.xlsx;*.xls"),("All files", "*.*")))
    else:
        # get the filepath returned by the filedialogue
        fname = askopenfilename(filetypes=(("Comma Separated Values Files", "*.csv"),("All files", "*.*")))
    if fname=="": # if the user pressed 'cancel' don't set the textbox to an empty string
        return
    
    entry.config(state=tk.NORMAL) # make the textbox un-greyed out so its value can be changed
    entry.delete(0,tk.END) # delete the previous textbox content
    entry.insert(0, fname) # set the textbox content to the file path
    entry.config(state=tk.DISABLED) # grey it out again
    
def enter_info():
    "The function that defines all the UI for the screen where info is entered"
    # clear the screen
    for w in root.winfo_children():
            w.destroy()
    # create the title
    tk.Label(text="Exam Spreadsheet",font=("Arial",50)).grid(row=0,column=0,columnspan=2,pady=20)
    
    # create the text which says what all the textboxes are for
    questionTexts = ("Exams and Students file?",
                     "Access arrangements file?",
                     "Exam dates file?",
                     "Output file name?",
                     "Clashes file (date ordered only)?")
    
    # call the grid geometry manager on the labels
    # this needs to be done as:
    #    they won't display otherwise
    #    their positions needs to be defined
    # sticky=tk.W makes them align to the left side of the sceen
    # padx=10 puts 10 pixels of space on either side of them
    for index, text in enumerate(questionTexts):
        tk.Label(text=text).grid(row=index+1,column=0,sticky=tk.W,padx=10)
    
    # creates the textboxes
    defaultVals = ('full data', 'access arrangements', 'exam times and dates', 'output', 'Clashes')
    textBoxes = []
    for index, value in enumerate(defaultVals):
        e=tk.Entry()
        e.insert(tk.END,value)
        e.config(state=tk.DISABLED)
        e.grid(row=index+1,column=1,sticky=tk.W+tk.E)
        textBoxes.append(e)
    
    # create browse buttons
    needsXSLX = (False,False,False,True,False)
    for index, textBox in enumerate(textBoxes):
        tk.Button(text="Browse",command=lambda textBox=textBox, xslx=needsXSLX[index]:browseFiles(textBox,xslx)).grid(row=index+1,column=2)
    
    # creates the submit button
    # command=lambda:submit(e1,e2,e3,e4,e5) means that when it is clicked, the submit function is called,
    # passing the textboxes as arguments to their content can be accessed
    submitButton = tk.Button(root,text="Submit",command=lambda: submit(*textBoxes))
    submitButton.grid(row=6,column=0,columnspan=2,sticky=tk.W+tk.E, pady=10, padx=10)
    helpButton = tk.Button(root,text="Help",command=helpBox)
    helpButton.grid(row=6,column=2,columnspan=1,sticky=tk.W+tk.E, pady=10, padx=10)
    root.update()
    root.minsize(root.winfo_width(), root.winfo_height())

def closingScreen():
    "Defines the UI of the closing screen"
    # clears the screen
    for w in root.winfo_children():
            w.destroy()
    # creates the text and buttons
    # uses the pack geometry manager as it is simple UI and doesn't require the fine control that the grid geometry manager allows
    tk.Label(root, text="Do you want to continue or close the program?").pack(side=tk.TOP)
    tk.Button(root,text="Open worksheet",command=openWorksheet).pack(side=tk.LEFT) # if this button is clicked, call the openWorksheet fuction
    tk.Button(root,text="Exit",command=lambda:root.destroy()).pack(side=tk.RIGHT) # if this button is clicked, close the window

def openWorksheet():
    "Open the worksheet in the default spreadsheet program"
    commandString = 'start ""  "'+values[3]+'.xlsx"'
    command(commandString)
    root.destroy()

# the queue that will be used to sent signals outsite the thread
queue1=queue.Queue()
def errorMessageLoop():
    "The function that will be repeatedly called to check for items in the queue and behave based on the value from the queue"
    try:
        # get 1 value from the queue
        # the default behaviour of a queue when there are no items is to wait until there is an item and return that:
        # this would freeze the UI of the program and it would only un-freeze when an item is passed in the queue
        # this is why the block keyword argument must be False so if the queue is empty it raises a queue.Empty error
        # and doesn't freeze the program
        val = queue1.get(block=False)
        
        if val=="finished": # if the value passes in to the queue is the string "finished"
            closingScreen() # create the UI for the closing screen
        else:
            # the only values passed in to the queue are the string "finished" or an error object
            # as the value isn't "finished" it must be an error encoutered when running the program
            # so can be raised
            # this will create the error encoutered in the program here instead
            raise val
    except queue.Empty: # if the queue is empty
        pass # do nothing
    except PermissionError:
        messagebox.showerror("Error","Errors with permission error. Please close the output file")
        enter_info()
    except FileNotFoundError:
        messagebox.showerror("Error","A file doesn't exist. Please enter correct file names/directories")
        enter_info()
    except ZeroDivisionError:
        messagebox.showerror("Error","Something divided by 0")
        enter_info()
    except Exception as e:
        messagebox.showerror("Error","An unknown error occured:\n"+str(e))
    root.after(100,errorMessageLoop)

def start():
    "Start relevant functions to get the UI for this program running"
    global root
    root = tk.Tk() # create a window
    root.wm_title("Exam Speadsheet") # set the title
    root.columnconfigure(1,weight=1) # make sure the UI takes up the full width of the screen
    errorMessageLoop() # start the loop that fetches items from the queue
    enter_info() # set up UI of the opening screen
    root.mainloop() # start processing events
    
if __name__ == "__main__":
    start()
